package sisbiblioteca.modelo;

import java.util.ArrayList;

public class Biblioteca 
{
	private String nome;
	private String endereco;
	//O objeto "livros" é um ArrayList (uma lista/vetor dinamico)
	//de objetos da classe livro
	private ArrayList<Livro> livros;
	
	public Biblioteca(String nome) 
	{
		this.nome = nome;
		this.endereco = "NÃO INFORMADO";
		//Quando instanciamos um objeto da classe Bibilioteca,
		//iniciamos sua lista de livros (que comeca vazia)
		this.livros = new ArrayList<>();
	}
	
	public void setNome(String nome) 
	{
		if (!nome.isEmpty()) 
		{
			this.nome = nome;
		}
	}
	
	public String getNome()
	{
		return this.nome.toUpperCase();
	}
	
	public void setEndereco(String endereco) 
	{
		if (!endereco.isEmpty()) 
		{
			this.endereco = endereco;
		}
	}
	
	public String getEndereco() 
	{
		return this.endereco.toUpperCase();
	}
	
	public void incluirLivro(Livro livro) 
	{
		this.livros.add(livro);
	}
	
	public boolean removerLivro(Livro livro) 
	{
		return this.livros.remove(livro);
	}
	
	public Livro obterLivro(int posicao) 
	{
		return livros.get(posicao);
	}
	
	public int tamanhoDoAcervo() 
	{
		return livros.size();
	}
	
	public String listarAcervo() 
	{
		String acervo = "ACERVO " + getNome() + "\n"
			+ "Quantidade: " + tamanhoDoAcervo() + "\n\n";
		for (Livro livro : livros) 
		{
			acervo = acervo + livro + "\n";
		}
		
		return acervo;
	}
}
