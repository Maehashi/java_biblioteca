package sisbiblioteca.modelo;

public class Livro {

	private int codigo;
	private String titulo;
	private int numeroDePaginas;
	private Autor autor;
	
	private static int contador = 0;
	
	public Livro(String titulo) 
	{
		contador++;
		this.codigo = contador;
		this.titulo = titulo;
	}
	
	public void setTitulo(String titulo) 
	{
		// O título do livro somente será vazio se NÃO for vazio
		if (!titulo.isEmpty()) 
		{
			this.titulo = titulo;
		}
	}
	
	public int getCodigo() 
	{
		return this.codigo;
	}

	public String getTitulo() 
	{
		// O titulo sera retornado
		return this.titulo.toUpperCase();
	}

	public void setNumeroDePaginas(int numeroDePaginas) 
	{
		// Só mudamos se for maior que zero
		if (numeroDePaginas > 0) 
		{
			this.numeroDePaginas = numeroDePaginas;
		}
	}

	public int getNumeroDePaginas() 
	{
		return numeroDePaginas;
	}

	public void setAutor(Autor autor) 
	{
		this.autor = autor;
	}

	public Autor getAutor() 
	{
		if (this.autor == null) 
		{
			return new Autor("Não informado");
		}
		return this.autor;
	}
	
	
	public String toString() 
	{
		return "FICHA CATALOGRAFICA DO LIVRO CODIGO" + getCodigo() + "\n"
				+ "Titulo: " + getTitulo() + "\n"
				+ getAutor() + "\n"
				+ "Numero de Pag: " + getNumeroDePaginas() + "\n";
	}
}


