package sisbiblioteca.view;

import javax.swing.JOptionPane;

import sisbiblioteca.modelo.*;


public class Principal
{
	public static void main (String[] args) 
	{
		Autor autor1 = new Autor("J.K. Rowling");
		Autor autor2 = new Autor("Stephen King");
		Autor autor3 = new Autor("Douglas Adams");
		
		Livro livro1 = new Livro("Harry Potter e a Camera Secreta");
		livro1.setAutor(autor1);
		livro1.setNumeroDePaginas(342);
		
		Livro livro2 = new Livro ("It");
		livro2.setAutor(autor2);
		livro2.setNumeroDePaginas(345);
		
		Livro livro3 = new Livro("Corinthinas Campeão");
		livro3.setAutor(autor3);
		livro3.setNumeroDePaginas(221);
		
		
		//JOptionPane.showMessageDialog(null, livro1);
		//JOptionPane.showMessageDialog(null, livro2);
		//JOptionPane.showMessageDialog(null, livro3);
		
		Biblioteca biblio1 = new Biblioteca("BIBLIOTECA FATEC POMPEIA");
		biblio1.setEndereco("FUNDAÇÃO SHUNJI NISHIMURA");
		biblio1.incluirLivro(livro1);
		biblio1.incluirLivro(livro3);
		
		JOptionPane.showMessageDialog(null, biblio1.getNome()
				+ "possui " + biblio1.tamanhoDoAcervo() + "livro(s)"
				+ "em seu acervo.");
		
		Biblioteca biblio2 = new Biblioteca("BIBLIOTECA FATEC GARÇA");
		biblio2.setEndereco("Av. Presidente Vargas");
		biblio2.incluirLivro(livro2);
		
		
	JOptionPane.showMessageDialog(null, biblio2.getNome()
			+ "possui " + biblio2.tamanhoDoAcervo() + "livro(s)"
			+ "em seu acervo.");
	
	JOptionPane.showMessageDialog(null, biblio1.listarAcervo());
	JOptionPane.showMessageDialog(null, biblio2.listarAcervo());
	}
	
}

