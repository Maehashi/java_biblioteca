package sisbiblioteca.modelo;

public class Autor {
	
	private String nome;
	
	public Autor(String nome) 
	{
		this.nome = nome;
	}
	
	public void setNome(String nome) 
	{
		//Somente difine o nome se ele não (!) for vazio
		if (!nome.isEmpty()) 
		{
			this.nome = nome;
		}
	}

	public String getNome() 
	{
		//Retorna o nome do autor em letras maiúsculas
		return this.nome.toUpperCase();
	}
	
	public String toString() 
	{
		return "Nome do Autor: " + getNome();
	}
}
